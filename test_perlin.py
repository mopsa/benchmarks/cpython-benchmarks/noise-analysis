import unittest

from noise._perlin import noise1, noise2, noise3

class PerlinTestCase(unittest.TestCase):

    def test_perlin_1d_range(self):
        for i in range(-10000, 10000):
            x = i * 0.49
            n = noise1(x)
            self.assertTrue(-1.0 <= n <= 1.0, (x, n))

    def test_perlin_1d_octaves_range(self):
        for i in range(-1000, 1000):
            for o in range(10):
                x = i * 0.49
                n = noise1(x, octaves=o + 1)
                self.assertTrue(-1.0 <= n <= 1.0, (x, n))

    def test_perlin_1d_base(self):
        self.assertEqual(noise1(0.5), noise1(0.5, base=0))
        self.assertNotEqual(noise1(0.5), noise1(0.5, base=5))
        self.assertNotEqual(noise1(0.5, base=5), noise1(0.5, base=1))

    def test_perlin_2d_range(self):
        for i in range(-10000, 10000):
            x = i * 0.49
            y = -i * 0.67
            n = noise2(x, y)
            self.assertTrue(-1.0 <= n <= 1.0, (x, y, n))

    def test_perlin_2d_octaves_range(self):
        for i in range(-1000, 1000):
            for o in range(10):
                x = -i * 0.49
                y = i * 0.67
                n = noise2(x, y, octaves=o + 1)
                self.assertTrue(-1.0 <= n <= 1.0, (x, n))

    def test_perlin_2d_base(self):
        x, y = 0.73, 0.27
        self.assertEqual(noise2(x, y), noise2(x, y, base=0))
        self.assertNotEqual(noise2(x, y), noise2(x, y, base=5))
        self.assertNotEqual(noise2(x, y, base=5), noise2(x, y, base=1))

    def test_perlin_3d_range(self):
        for i in range(-10000, 10000):
            x = -i * 0.49
            y = i * 0.67
            z = -i * 0.727
            n = noise3(x, y, z)
            self.assertTrue(-1.0 <= n <= 1.0, (x, y, z, n))

    def test_perlin_3d_octaves_range(self):
        for i in range(-1000, 1000):
            x = i * 0.22
            y = -i * 0.77
            z = -i * 0.17
            for o in range(10):
                n = noise3(x, y, z, octaves=o + 1)
                self.assertTrue(-1.0 <= n <= 1.0, (x, y, z, n))

    def test_perlin_3d_base(self):
        x, y, z = 0.1, 0.7, 0.33
        self.assertEqual(noise3(x, y, z), noise3(x, y, z, base=0))
        self.assertNotEqual(noise3(x, y, z), noise3(x, y, z, base=5))
        self.assertNotEqual(noise3(x, y, z, base=5), noise3(x, y, z, base=1))


if __name__ == '__main__':
    unittest.main()
