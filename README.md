This repository is a copy of the [noise library](https://github.com/caseman/noise), using commit bb32991ab97e90882d0e46e578060717c5b90dc5.

Run `make` to build the library using `mopsa-build` and launch the analysis of the tests.
The results are written in text files by default.

Minor changes performed to analyze the library are shown in CHANGELOG.md
