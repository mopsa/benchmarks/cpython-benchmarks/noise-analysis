MOPSA_BIN?=$(shell dirname $(shell which mopsa))
MOPSA_OPTIONS?=
MOPSA_ARGS=${MOPSA_OPTIONS} -silent
OUTPUT_DIR?=.

ifneq (,$(findstring json,$(MOPSA_ARGS)))
	MOPSA_OUT_EXT=.json
else
	MOPSA_OUT_EXT=.txt
endif


UNITTEST_FILE=test_perlin.py
UNITTEST_NAME=$(shell basename -s .py ${UNITTEST_FILE})
UNITTEST_OUT=${OUTPUT_DIR}/noise_${UNITTEST_NAME}${MOPSA_OUT_EXT}

UNITTEST_FILE2=test_simplex.py
UNITTEST_NAME2=$(shell basename -s .py ${UNITTEST_FILE2})
UNITTEST_OUT2=${OUTPUT_DIR}/noise_${UNITTEST_NAME2}${MOPSA_OUT_EXT}


all: build unittests

build:
	mopsa-build python3 setup.py build_ext --force > build_log 2>&1

unittests:
	mkdir -p ${OUTPUT_DIR}

	if [ -f ${UNITTEST_OUT} ]; then mv ${UNITTEST_OUT} ${UNITTEST_OUT}.old; fi;
	${MOPSA_BIN}/mopsa-cpython ${MOPSA_ARGS} ${UNITTEST_FILE} 1>${UNITTEST_OUT} 2>${OUTPUT_DIR}/noise_${UNITTEST_NAME}.stderr

	if [ -f ${UNITTEST_OUT2} ]; then mv ${UNITTEST_OUT2} ${UNITTEST_OUT2}.old; fi;
	${MOPSA_BIN}/mopsa-cpython ${MOPSA_ARGS} ${UNITTEST_FILE2} 1>${UNITTEST_OUT2} 2>${OUTPUT_DIR}/noise_${UNITTEST_NAME2}.stderr

ci:
	mopsa-build python3 setup.py build_ext --force
	mopsa-db -v
	mopsa-cpython ${MOPSA_ARGS} ${UNITTEST_FILE}
	mopsa-cpython ${MOPSA_ARGS} ${UNITTEST_FILE2}

.PHONY: build unittests
