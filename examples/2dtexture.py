"""Writes a 256x256 grayscale simplex noise texture file in pgm format
(see http://netpbm.sourceforge.net/doc/pgm.html)
"""
# $Id: 2dtexture.py 21 2008-05-21 07:52:29Z casey.duncan $

import sys
from noise._simplex import noise2 as snoise2

if len(sys.argv) not in (2, 3) or '--help' in sys.argv or '-h' in sys.argv:
	print('2dtexture.py FILE [OCTAVES]')
	print()
        #	print(__doc__)
	raise SystemExit

# f = open(sys.argv[1], 'w')
if len(sys.argv) > 2:
	octaves = int(sys.argv[2])
else:
	octaves = 1
freq = 16.0 * octaves
print('P2\n')
print('256 256\n')
print('255\n')
for y in range(256):
	for x in range(256):
		print("%s\n" % int(snoise2(x / freq, y / freq, octaves) * 127.0 + 128.0))
