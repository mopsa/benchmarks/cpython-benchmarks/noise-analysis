import unittest

from noise._simplex import noise2, noise3, noise4

class SimplexTestCase(unittest.TestCase):

    def test_simplex_2d_range(self):
        for i in range(-10000, 10000):
            x = i * 0.49
            y = -i * 0.67
            n = noise2(x, y)
            self.assertTrue(-1.0 <= n <= 1.0, (x, y, n))

    def test_simplex_2d_octaves_range(self):
        for i in range(-1000, 1000):
            for o in range(10):
                x = -i * 0.49
                y = i * 0.67
                n = noise2(x, y, octaves=o + 1)
                self.assertTrue(-1.0 <= n <= 1.0, (x, n))

    def test_simplex_3d_range(self):
        for i in range(-10000, 10000):
            x = i * 0.31
            y = -i * 0.7
            z = i * 0.19
            n = noise3(x, y, z)
            self.assertTrue(-1.0 <= n <= 1.0, (x, y, z, n))

    def test_simplex_3d_octaves_range(self):
        for i in range(-1000, 1000):
            x = -i * 0.12
            y = i * 0.55
            z = i * 0.34
            for o in range(10):
                n = noise3(x, y, z, octaves=o + 1)
                self.assertTrue(-1.0 <= n <= 1.0, (x, y, z, o+1, n))

    def test_simplex_4d_range(self):
        for i in range(-10000, 10000):
            x = i * 0.88
            y = -i * 0.11
            z = -i * 0.57
            w = i * 0.666
            n = noise4(x, y, z, w)
            self.assertTrue(-1.0 <= n <= 1.0, (x, y, z, w, n))

    def test_simplex_4d_octaves_range(self):
        for i in range(-1000, 1000):
            x = -i * 0.12
            y = i * 0.55
            z = i * 0.34
            w = i * 0.21
            for o in range(10):
                n = noise4(x, y, z, w, octaves=o + 1)
                self.assertTrue(-1.0 <= n <= 1.0, (x, y, z, w, o+1, n))



if __name__ == '__main__':
    unittest.main()
